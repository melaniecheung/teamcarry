package com.example.teamcarry;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SavedListAdapter extends RecyclerView.Adapter<SavedListAdapter.MyViewHolder> {

    FoodList savedList = FoodList.getInstance();
    ArrayList<Food> savedArrayList = savedList.getSavedList();
    ArrayList<Food> foodArrayList = savedList.getFoodList();
    ArrayList<Food> toBeAddedArrayList = savedList.getToBeAddedList();

    public SavedListAdapter(){}

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView savedItemTxt;
        private ImageButton savedItemBox;
        private ImageButton deleteSavedButton;

        public MyViewHolder(final View view) {
            super(view);
            savedItemTxt = view.findViewById(R.id.savedItemTxt);
            savedItemBox = view.findViewById(R.id.savedItemBox);
            deleteSavedButton = view.findViewById(R.id.deleteSavedButton);
        }
    }

        @NonNull
        @Override
        public SavedListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View savedListView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_saved_item, parent, false);
            return new SavedListAdapter.MyViewHolder(savedListView);
        }

        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            String name = savedArrayList.get(position).getName();
            holder.savedItemTxt.setText(name);

            holder.deleteSavedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savedArrayList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, getItemCount());
                }
            });

            holder.savedItemBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (toBeAddedArrayList.contains(savedArrayList.get(position))){
                        holder.savedItemBox.setImageResource(R.drawable.saveditemunchecked);
                        toBeAddedArrayList.remove(savedArrayList.get(position));
                    }
                    else {
                        holder.savedItemBox.setImageResource(R.drawable.saveditemchecked);
                        toBeAddedArrayList.add(savedArrayList.get(position));
                    }
                }
            });


        }

        @Override
        public int getItemCount() {
            return savedArrayList.size();
    }


}