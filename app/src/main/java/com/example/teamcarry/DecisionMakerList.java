package com.example.teamcarry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class DecisionMakerList extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decision_maker_list);

        FoodList foodList = FoodList.getInstance();
        ArrayList<Food> foodArrayList = foodList.getFoodList();
        if(foodArrayList.size() == 0){
            startActivity(new Intent(DecisionMakerList.this, DecisionInput.class));
        }

        recyclerView = findViewById(R.id.foodListRecyclerView);
        setAdapter();
        TextView isEmptyMessage = findViewById(R.id.isEmptyMessage);

        //press shuffle button go to shuffling... page
        ImageButton myButton3 = findViewById(R.id.shuffle);
        myButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(foodArrayList.size()>=2){
                    startActivity(new Intent(DecisionMakerList.this,ShufflingFood.class));
                }
                else {
                    isEmptyMessage.setText("More than 1 item is needed to shuffle!");
                }
            }
        });

        ImageButton addItemButton = findViewById(R.id.addItem);
        addItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DecisionMakerList.this, DecisionInput.class));
            }
        });

        ImageButton viewSaved = findViewById(R.id.viewSavedButton);

        viewSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                startActivity(new Intent(DecisionMakerList.this, SavedListView.class));
            }
        });

        ImageButton backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DecisionMakerList.this, MainActivity.class));
            }
        });
    }

    private void setAdapter(){
        FoodListRecyclerViewAdapter adapter = new FoodListRecyclerViewAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        FoodList foodLists = FoodList.getInstance();
        ArrayList<Food> foodList = foodLists.getFoodList();

        ImageButton clearAllButton = findViewById(R.id.clearAll);
        clearAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = foodList.size();
                foodList.clear();
                adapter.notifyItemRangeChanged(0, size);
                adapter.notifyItemRangeRemoved(0, size);

            }
        });
    }
}