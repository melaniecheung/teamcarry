package com.example.teamcarry;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.teamcarry.Food;
import com.example.teamcarry.R;

public class FoodDetailsActivity extends AppCompatActivity {
    public static final String EXTRA_FOOD = "extra_food";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_details);



        ImageView imageView = findViewById(R.id.img_detail);
        TextView name = findViewById(R.id.tv_name_detail);
        TextView description = findViewById(R.id.tv_desc_detail);
        TextView elevation = findViewById(R.id.tv_elevation_detail);
        TextView country = findViewById(R.id.tv_country_detail);
        TextView steps = findViewById(R.id.steps);

        Food food = getIntent().getParcelableExtra(EXTRA_FOOD);

        String uri = food.getPhoto();
        int imageResource = this.getResources().getIdentifier(uri, null, this.getPackageName());
        Glide.with(this).load(imageResource).into(imageView);


        name.setText(food.getName());
        description.setText(food.getDescription());
        elevation.setText(food.getElevation());
        country.setText(food.getLocation());
        steps.setText(food.getSteps());
        ;    }
}
