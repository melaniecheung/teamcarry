package com.example.teamcarry;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FoodListRecyclerViewAdapter extends RecyclerView.Adapter<FoodListRecyclerViewAdapter.MyViewHolder>{

    FoodList foodList = FoodList.getInstance();
    ArrayList<Food> foodArrayList = foodList.getFoodList();

    public FoodListRecyclerViewAdapter(){}

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView foodNameTxt;
        private ImageButton deleteFoodButton;

        public MyViewHolder(final View view){
            super(view);
            foodNameTxt = view.findViewById(R.id.foodName);
            deleteFoodButton = view.findViewById(R.id.deleteButton);
        }
    }

    @NonNull
    @Override
    public FoodListRecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View foodListView = LayoutInflater.from(parent.getContext()).inflate(R.layout.decision_maker_item, parent, false);
        return new MyViewHolder(foodListView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String name = foodArrayList.get(position).getName();
        holder.foodNameTxt.setText(name);

        holder.deleteFoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foodArrayList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, getItemCount());
            }
        });
    }

    @Override
    public int getItemCount() {
        return foodArrayList.size();
    }

}