package com.example.teamcarry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Size;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;

import java.util.Random;

public class FinalResult extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_result);
        FoodList foodList = FoodList.getInstance();
        ArrayList<Food> foodArrayList = foodList.getFoodList();

        // get random food from foodArrayList
        Random rand = new Random();
        int n = rand.nextInt(foodArrayList.size() - 1);

        String foodName = foodArrayList.get(n).getName();

        TextView finalResultTxt = findViewById(R.id.finalresult);

        finalResultTxt.setText(foodName);

        ImageButton myButton = findViewById(R.id.homebutton);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodArrayList.clear();
                navigateToHome();
            }
        });

        ImageButton myButton2 = findViewById(R.id.idontlikethis);
        myButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToDecisionMaker();
            }
        });

    }

    private void navigateToHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void navigateToDecisionMaker() {
        Intent intent = new Intent(this, DecisionMakerList.class);
        startActivity(intent);
    }
}