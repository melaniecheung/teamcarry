package com.example.teamcarry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class DecisionInput extends AppCompatActivity {

    boolean isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.decisionmaker);

        FoodList list = FoodList.getInstance();
        ArrayList<Food> inputFoodList = list.getFoodList();
        ArrayList<Food> savedFoodList = list.getSavedList();

        TextView foodNameTextView =  findViewById(R.id.inputfood);
        String foodName = foodNameTextView.getText().toString();


        CheckBox addToSaved = findViewById(R.id.addtosaved);
        addToSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (addToSaved.isChecked()){
                    isChecked = true;
                }
                else {
                    isChecked = false;
                }
            }
        });

        ImageButton submit = findViewById(R.id.enterbutton);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String foodName = foodNameTextView.getText().toString();
                Food food = new Food();
                food.setName(foodName);

                if (isChecked) {
                    savedFoodList.add(food);
                }

                if (foodName.isEmpty()){
                }
                else {
                    inputFoodList.add(food);
                    startActivity(new Intent(DecisionInput.this, DecisionMakerList.class));
                }
            }
        });

}}