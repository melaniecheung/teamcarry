package com.example.teamcarry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // navigate to app swipe screen
        Button myButton = findViewById(R.id.browse);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToAppSwipeScreen();
            }
        });

        // navigate to recipe display to test if scrollview is working
        Button myButton2 = findViewById(R.id.cardshuffle);
        myButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToRecipeDisplay();
            }
        });
        ImageButton breakfastButton = findViewById(R.id.Breakfast);
        breakfastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToBreakfast();
            }
        });

        ImageButton dinnerButton = findViewById(R.id.Dinner);
        dinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToDinner();
            }
        });

        ImageButton lunchButton = findViewById(R.id.Lunch);
        lunchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToLunch();
            }
        });

    }

    // app swipe intent
    private void navigateToAppSwipeScreen() {
        Intent intent = new Intent(this, BrowseRecipesActivity.class);
        startActivity(intent);
    }


    // recipe display intent
    private void navigateToRecipeDisplay() {
        Intent intent = new Intent(this, DecisionMakerList.class);
        startActivity(intent);
    }

    private void navigateToBreakfast() {
        Intent intent = new Intent(this, Breakfast.class);
        startActivity(intent);
    }

    private void navigateToDinner() {
        Intent intent = new Intent(this, Dinner.class);
        startActivity(intent);
    }

    private void navigateToLunch() {
        Intent intent = new Intent(this, Lunch.class);
        startActivity(intent);
    }

}