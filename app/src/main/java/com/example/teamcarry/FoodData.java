package com.example.teamcarry;

import java.util.ArrayList;


public class FoodData {

    // array to store data that will be displayed on RecyclerView
    public static String[][] data = new String[][] {
            {"Instant Ramen",
                    "1",
                    "drawable/foodimage",
                    "These Instant Pot ramen noodles are SO EASY to make, healthier than the packaged version, and make an excellent afternoon or late night snack! Add some cooked chicken, steak, or pork to make it a full meal.",
                    "- 1 ramen 'cake'\n- 1 1/2 cup chicken stock\n- 2 tsp soy sauce\n- 1 tsp sesame oil\n- 1 tsp dried minced onion\n- 1 tsp granulated garlic\n- 1/2 tsp ground ginger\n- 1 thinly sliced mushroom\n- Optional: chopped green onion",
                    "1. Pour the stock in the Instant Pot. Place in the ramen cake. Add the remaining ingredients on top of the ramen. Place the lid on your Instant Pot, turn the valve to sealing, and set the cook time to 1 minute on high pressure.\n\n2. When the cook time is complete, quick release the pressure. Carefully remove the lid, the toss the ramen noodles with a fork to loosen them and mix them together with the broth.\n\n3. Pour the ramen soup into a bowl and enjoy. (Optional: top with chopped green onion)."},
            {"Poke Bowl",
                    "4 bowls",
                    "drawable/pokebowl1",
                    "This easy Poke Bowl Recipe  is packed with sushi-grade ahi tuna seasoned with soy, honey, and plenty of sesame. It's served with sticky brown rice, tons of veggies and these easiest spicy mayo on the planet.",
                    "1 lb sushi-grade ahi tuna (or other fish of your choice)\n2 tbsp soy sauce\n1 tbsp sesame oil\n1 tbsp rice vinegar\n1 tsp honey\n1/4 cup light mayo\n1 tsp sriracha\n4 cups cooked brown rice\n1 cup diced cucumber\n1/2 cup shredded carrots\n1/2 cup shelled edamame\n2 large avocados, peeled and sliced\n1 tbsp black sesame seeds\n1 tsp green onion",
                    "1. Use a sharp knife to cut tuna into a dice. Add tuna, soy sauce, sesame oil, rice vinegar, and honey to a medium bowl. Toss to combine. Let the tuna sit while you prepare the rest of the ingredients.\n\n2. Add mayo and sriracha to a bowl. Stir to combine. Season with salt and pepper. Soon into a ziploc bag. Cut the tip off.\n\n3.Divide cooked rice between four bowls. Spoon tuna on one part of the rice. Surround with a pile of the cucumber, edamame, and carrot. Spread half of an avocado on top of the bowl. Drizzle the spicy mayo over the bowl. Sprinkle with green onion and sesame seeds."},
            {"Fried Rice",
                    "4-6 servings",
                    "drawable/friedrice",
                    "Learn how to make fried rice with this classic recipe. It only takes 15 minutes to make, it’s easy to customize with your favorite add-ins, and it’s SO flavorful and delicious!",
                    "3 tbsp butter\n2 eggs, whisked\n2 medium carrots, peeled and diced\n1 small white onion, diced\n1/2 cup frozen peas\nsalt and black pepper\n4 cups cooked and chilled rice\n3 green onions, thinly sliced\n3-4 tbsp soy sauce\n2 tsp oyster sauce (optional)\n1/2 tsp toasted sesame oil",
                    "1. Heat 1/2 tablespoon of butter in a large sauté pan* over medium-high heat until melted. Add egg, and cook until scrambled, stirring occasionally. Remove egg, and transfer to a separate plate.\n\n2. Add an additional 1 tablespoon butter to the pan and heat until melted. Add carrots, onion, peas and garlic, and season with a generous pinch of salt and pepper. Sauté for about 5 minutes or until the onion and carrots are soft. Increase heat to high, add in the remaining 1 1/2 tablespoons of butter, and stir until melted. Immediately add the rice, green onions, soy sauce and oyster sauce (if using), and stir until combined. Continue sautéing for an additional 3 minutes to fry the rice, stirring occasionally.  (I like to let the rice rest for a bit between stirs so that it can crisp up on the bottom.)  Then add in the eggs and stir to combine. Remove from heat, and stir in the sesame oil until combined.  Taste and season with extra soy sauce, if needed.\n\n3. Serve immediately, or refrigerate in a sealed container for up to 3 days."},
            {"Spaghetti",
                    "8 servings",
                    "drawable/spag",
                    "This family recipe was passed down from generation to generation. Definitely a family favourite!",
                    "1 lb ground-beef\n1 onion, chopped\n4 cloves garlic, minced\n1 small green bell pepper, diced\n1 can diced tomatoes\n1 can tomato sauce\n1 can tomato paste\n2 tsp dried oregano\n2 tsp dried basil\n1 tsp salt\n1/2 tsp black pepper",
                    "1. Combine ground beef, onion, garlic, and green pepper in a large saucepan. Cook and stir until meat is brown and vegetables are tender. Drain grease.\n\n2. Stir diced tomatoes, tomato sauce, and tomato paste into the pan. Season with oregano, basil, salt, and pepper. Simmer spaghetti sauce for 1 hour, stirring occasionally."},
            {"Chicken Curry",
                    "4 servings",
                    "drawable/chickencurry",
                    "Chicken Curry is an aromatic chicken dish in a rich curry sauce with coconut served over white rice.",
                    "1 lb chicken breast\n1 tbsp vegetable oil\n1 tbsp garlic minced\n1 small yellow or white onion chopped\n2 tbsp yellow curry powder\n1 tbsp Thai red curry paste\n15 oz coconut milk canned\n1/2 cup water or chicken stock\n1 tbsp brown sugar\n1 teaspoon fish sauce\n2 tbsp lime juice\nsalt to taste\nhandful fresh cilantro chopped\n4 cups cooked white rice",
                    "1. Heat the oil in a large pot over medium low heat. Add the onions and minced garlic and cook for a few minutes until the onions are fragrant and softened.\n\n2. Add the chicken and cook for 2-3 minutes, browning it a little. Add curry powder and paste; cook for 3-5 minutes.\n\n3. Add the coconut milk, and let simmer for 15-20 minutes or until the chicken is fully cooked.\n\n4. Add water or chicken stock depending on the consistency ou want for the sauce, or let simmer longer to thicken if needed.\n\n5. Stir in the brown sugar, fish sauce, and lime juice. Taste and salt if needed.\n\n6. Top with fresh cilantro, serve over cooked rice."},
            {"Beef Tacos",
                    "10 servings",
                    "drawable/beeftaco",
                    "Tacos are as easy as 1, 2, 3! Olé! By Betty Crocker Kitchens",
                    "1 lb lean ground beef\n1 cup Old El Paso™ Thick 'n Chunky salsa\n10 Old El Paso™ taco shells\n1/2 head lettuce, shredded\n1 medium tomato, chopped (3/4 cup)\n1 cup shredded Cheddar cheese (4 oz)",
                    "1. Cook beef in 10-inch skillet over medium heat 8 to 10 minutes, stirring occasionally, until brown; drain.\n\n2. Stir salsa into beef. Heat to boiling, stirring constantly; reduce heat to medium-low. Cook 5 minutes, stirring occasionally. Pour beef mixture into large serving bowl.\n\n3. Heat taco shells as directed on package. Serve taco shells with beef mixture, lettuce, tomato and cheese."},
            {"Spring Rolls",
                    "8 rolls",
                    "drawable/vietspringroll",
                    "These spring rolls are a refreshing change from the usual fried variety, and have become a family favorite. They are great as a cool summertime appetizer, and are delicious dipped in one or both of the sauces.",
                    "2 oz rice vermicelli\n8 rice wrappers (8.5 inch diameter)\n8 large cooked shrimp - peeled, deveined and cut in half\n1 1/3 tbsp chopped fresh Thai basil\n3 tbsp chopped fresh mint leaves\n3 tbsp chopped fresh cilantro\n2 leaves lettuce, chopped\n4 teaspoons fish sauce\n1/4 cup water\n2 tbsp fresh lime juice\n1 clove garlic, minced\n2 tbsp white sugar\n1/2 teaspoon garlic chili sauce\n3 tbsp hoisin sauce\n1 tsp finely chopped peanuts",
                    "1. Cook beef in 10-inch skillet over medium heat 8 to 10 minutes, stirring occasionally, until brown; drain.\n\n2. Stir salsa into beef. Heat to boiling, stirring constantly; reduce heat to medium-low. Cook 5 minutes, stirring occasionally. Pour beef mixture into large serving bowl.\n\n3. Heat taco shells as directed on package. Serve taco shells with beef mixture, lettuce, tomato and cheese."},
            {"Teriyaki Chicken",
                    "4 servings",
                    "drawable/teriyakichicken",
                    "Teriyaki Chicken with broccoli is a super easy chicken recipe cooked in 10-minutes with no marinading needed! Crispy and juicy skinless chicken thighs stir-fried and swimming in a beautiful flavoured homemade teriyaki sauce. A hint of garlic adds a twist on a traditional Japanese Teriyaki. Better than bottled sauce!",
                    "1.3 lbs (600 grams) skinless boneless chicken thighs, cut into 1 1/2-inch pieces\n1 tbsp cooking oil\n1/4 cup low-sodium soy sauce\n3 tbsp light brown sugar or white granulated sugar\n3 tbsp Mirin\n1 tbsp sesame oil\n2 tsp minced garlic\n1 shallot/green onion stem, sliced to garnish\n4 cups broccoli florets, lightly steamed",
                    "1. Heat cooking oil in a large pan over medium heat. Stir fry chicken, stirring occasionally until lightly browned and crisp.\n\n2. In a small jug or bowl whisk together the soy sauce, sugar, Sake/vinegar, Mirin and sesame oil to combine. Set aside.\n\n3. Add the garlic to the centre of the pan and saute until lightly fragrant (about 30 seconds). Pour in the sauce and allow to cook, while stirring, until the sauce thickens into a beautiful shiny glaze (about 2-3 minutes).\n\nAdd in the steamed broccoli. Garnish with green onion (or shallot) slices and serve over steamed rice."},
    };

    public static ArrayList<Food> getListData() {
        Food food;
        ArrayList<Food> list = new ArrayList<>();

        for (String[] mData : data) {
            food = new Food();
            food.setName(mData[0]);
            food.setElevation(mData[1]);
            food.setPhoto(mData[2]);
            food.setDescription(mData[3]);
            food.setLocation(mData[4]);
            food.setSteps(mData[5]);

            list.add(food);
        }

        return list;
    }

}