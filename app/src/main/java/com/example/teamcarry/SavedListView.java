package com.example.teamcarry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;

public class SavedListView extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_list_view);

        FoodList savedList = FoodList.getInstance();
        ArrayList<Food> savedArrayList = savedList.getSavedList();
        ArrayList<Food> foodArrayList = savedList.getFoodList();
        ArrayList<Food> toBeAddedArrayList = savedList.getToBeAddedList();

        recyclerView = findViewById(R.id.savedList);
        setAdapter();

        ImageButton addSelected = findViewById(R.id.addSelected);

        addSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < toBeAddedArrayList.size(); i++) {
                    foodArrayList.add(toBeAddedArrayList.get(i));
                }
                startActivity(new Intent(SavedListView.this, DecisionMakerList.class));
                toBeAddedArrayList.clear();

            }
        });

        ImageButton backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SavedListView.this, DecisionMakerList.class));
            }
        });
    }

    private void setAdapter(){
        SavedListAdapter adapter = new SavedListAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }
}