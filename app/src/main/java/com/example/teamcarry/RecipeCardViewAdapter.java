package com.example.teamcarry;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecipeCardViewAdapter extends RecyclerView.Adapter<RecipeCardViewAdapter.CardViewHolder> {
    private Context context;
    private ArrayList<Food> listFood;

    public RecipeCardViewAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<Food> getListFood() {
        return listFood;
    }

    public void setListFood(ArrayList<Food> listFood) {
        this.listFood = listFood;
    }



    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse_recipes_cardview, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, final int position) {
        holder.tvName.setText(getListFood().get(position).getName());
        holder.tvDescription.setText(getListFood().get(position).getDescription());
        Glide.with(context).load(R.drawable.ramen2).into(holder.imgPhoto);
        String uri = getListFood().get(position).getPhoto();
        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
        Glide.with(context).load(imageResource).into(holder.imgPhoto);

        // intent parcel able to detail
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailActivity = new Intent(context, FoodDetailsActivity.class);
                detailActivity.putExtra(FoodDetailsActivity.EXTRA_FOOD, listFood.get(position));
                context.startActivity(detailActivity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return getListFood().size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvName, tvDescription;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_card);
            tvName = itemView.findViewById(R.id.tv_name_card);
            tvDescription = itemView.findViewById(R.id.tv_desc_card);
        }
    }
}
