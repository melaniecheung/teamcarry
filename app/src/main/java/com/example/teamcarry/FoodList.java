package com.example.teamcarry;

import java.util.ArrayList;

public class FoodList {

    private ArrayList<Food> foodList = new ArrayList<Food>();
    private ArrayList<Food> savedList = new ArrayList<Food>();
    private ArrayList<Food> toBeAddedList = new ArrayList<Food>();
    private static FoodList instance;

    private FoodList(){
    }

    public static FoodList getInstance(){

        if (instance == null){
            instance = new FoodList();
        }
        return instance;
    }

    public ArrayList<Food> getFoodList(){
        return foodList;
    }

    public ArrayList<Food> getSavedList(){
        return savedList;
    }

    public ArrayList<Food> getToBeAddedList(){
        return toBeAddedList;
    }


    public void addFood(Food food){
        foodList.add(food);
    }

    public void addSavedFood(Food food){
        savedList.add(food);
    }

    public void removeFood(Food food){
        foodList.remove(food);
    }

    public void removeSavedFood(Food food){
        savedList.remove(food);
    }
}
